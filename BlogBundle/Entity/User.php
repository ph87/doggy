<?php
// src/Doggy/BlogBundle/Entity/User.php
namespace Doggy\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Doggy\BlogBundle\Entity\User
 *
 * @ORM\Table(name="blog_user")
 *
 */
class User implements UserInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;

    /*
     * @ORM\Column(type="string", length=32)
     */
    private $salt;

    /*
     * @ORM\Column(type="string", length=40)
     */
    private $password;




    public function getRoles()
    {
        return array('ROLE_USER');
    }


    public function getPassword()
    {
        return $this->password;
    }


    public function getSalt()
    {
        return $this->salt;
    }


    public function getUsername()
    {
        return $this->username;
    }


    public function eraseCredentials()
    {
    }
}
