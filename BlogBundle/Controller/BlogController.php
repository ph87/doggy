<?php

namespace Doggy\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
/* $request = Request::createFromGlobals();
 * $request->getPathInfo();
 * $request->query->get('foo'); // GET
 * $request->request->get('bar', 'default value if bar does not exist'); //POST
 * $request->server-get('HTTP_HOST'); //retrieve SERVER variables
 * $request->files->get('foo'); //retrieves an instance of uploadeFile identified by foo
 * $request->cookies->get('PHPSESSID'); //retrieve a cookie value
 * $request->headers->get('host');
 * $request->headers->get('conotent_type');
 * $request->getMethod(); // retrieve the request method: GET, POST, PUT, DELETE, HEAD
 * $requets->getLanguages(); //an array of languages the client accepts
 */
use Symfony\Component\HttpFoundation\Response;
/* $response = new Response();
 * $response->setContent('<html>..</html>');
 * $response->setStatusCode(200);
 * $response->headers->set('Content-Type', 'text/html');
 */

use Doggy\BlogBundle\Entity\Blog;

class BlogController extends Controller
{
  public function wtfAction()
  {
    return new Response("你想干神马");
  }

  public function addAction()
  {
    $html = '<html><body><form action="add" method="POST"><p>title:<input type="text" name="title" /></p><p>author:<input type="text" name="author" /></p><p>tag:<input type="text" name="tag" /></p><p>content:<input type="textarea" name="content" /></p><p><input type="submit" /></p> </form></body></html>';
    $response = new Response();
    $response->setContent($html);
    $response->setStatusCode(200);
    $response->headers->set('Content-Type', 'text/html');
    return $response;
  }


  public function createAction(Request $request)
  {
    $title = $request->request->get('title', 'title');
    $author = $request->request->get('author', 'author');
    $tag = $request->request->get('tag', 'tag');
    $content = $request->request->get('content', 'content');

    $blog = new Blog();
    $blog->setTitle($title);
    $blog->setAuthor($author);
    //$blog->setDate(new \DateTime()->format('Y-m-d h:i:s'));
    $blog->setDate(new \DateTime(date("Y-m-d h:i:s")));
    $blog->setTag($tag);
    $blog->setContent($content);

    $em = $this->getDoctrine()->getManager();
    $em->persist($blog);
    $em->flush();
    return new Response('Create blog id'.$blog->getId());
  }

  public function listAction()
  {
    $blogs = $this->getDoctrine()
      ->getRepository("DoggyBlogBundle:Blog")
      ->findAll();
    if (!$blogs) {
      return new Response("什么都没有");
      //throw $this->createNotFoundException(
      //  'No blog found'
      //);
    }
    $html = '<html><body><ol>';
    foreach ($blogs as $blog){
      $id = $blog->getId();
      $title = $blog->getTitle();
      $date = $blog->getDate()->format('Y-m-d h:i:s');
      $url = $this->get('router')
        ->generate('doggy_blog_show', array('id'=>$id), true);
      //$url = "/".$id;
      $html = $html.'<li><a href="'.$url.'">'.$title.'</a> '.$date.' <form action="delete" method="POST"><input type="hidden" name="id" value="'.$id.'" /><input type="submit" value="delete" /></form></li>';
    }
    $html = $html.'</ol></body></html>';
    $response = new Response();
    $response->setContent($html);
    $response->setStatusCode(200);
    $response->headers->set('Content-Type', 'text/html');
    return $response;
  }

  public function showAction($id)
  {
    $blog = $this->getDoctrine()
      ->getRepository("DoggyBlogBundle:Blog")
      ->find($id);
    if (!$blog) {
      throw $this->createNotFoundException(
        'No blog found for id'.$id
      );
    }
    $title = $blog->getTitle();
    $author = $blog->getAuthor();
    $date = $blog->getDate()->format('Y-m-d h:i:s');
    $tag = $blog->getTag();
    $content = $blog->getContent();

    $html = '<html><body><b>'.$title.'</b><br />'.$date.'<br />'.$content.'<br /></body></html>';
    $response = new Response();
    $response->setContent($html);
    $response->setStatusCode(200);
    $response->headers->set('Content-Type', 'text/html');
    return $response;
  }

  public function updateAction($id)
  {
    $em = $this->getDoctrine()->getManager();
    $blog = $em->getRepository('DoggyBlogBundle:Blog')
      ->find($id);
    if (!$blog) {
      throw $this->createNotFoundException(
        'no blog found for id '.$id
      );
    }
    $blog->setTitle();
    $em->flush();
    return 'a';
  }
  public function deleteAction(Request $request)
  {
    $id = $request->request->get('id');
    $em = $this->getDoctrine()->getManager();
    $blog = $em->getRepository('DoggyBlogBundle:Blog')
      ->find($id);
    if (!$blog) {
     // throw $this->createNotFoundException(
     //   'no blog found for id '.$id
     // );
      return $this->redirect($this->generateUrl('doggy_blog_home'));

    }
    $em->remove($blog);
    $em->flush();
    return new Response("delete success");
  }

}

